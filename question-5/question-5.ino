const int potentiometerPin = 15; // Broche analogique utilisée pour le potentiomètre
const int ledPin = 9; // Broche utilisée pour la LED

void setup() {
  pinMode(ledPin, OUTPUT);
}

void loop() {
  int potValue = analogRead(potentiometerPin); // Lecture de la valeur du potentiomètre (entre 0 et 1023)
  int brightness = map(potValue, 0, 2046, 0, 255); // Mappage de la valeur à la plage de luminosité de la LED

  analogWrite(ledPin, brightness); // Réglage de la luminosité de la LED en fonction de la valeur du potentiomètre
}