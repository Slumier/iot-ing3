int ledPin = 2;
int handlePin = 0;

void setup() {
  Serial.begin(115200);
  pinMode(ledPin, OUTPUT);
}

void loop() {

  if (Serial.available() > 0) {

    char command = Serial.read(); 

    if (command == '1') {

      handlePin = 0;
      Serial.println("LED allumée");

    } else if (command == '0') {

      handlePin = 1;
      Serial.println("LED éteinte");
    }

    if ( handlePin == 1 ) {
      digitalWrite(ledPin, HIGH);
    } else {
      digitalWrite(ledPin, LOW);
    }

  }
}
